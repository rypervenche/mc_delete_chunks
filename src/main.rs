use anyhow::{bail, Result};
use clap::Parser;
use std::fmt;
use std::fs;
use std::path::{Path, PathBuf};

#[derive(Parser)]
#[clap(about = "Delete unused regions outside of given positive X and Z coordinates of rectangle")]
struct Args {
    #[clap(short, default_value = "21504")]
    x: usize,
    #[clap(short, default_value = "10752")]
    z: usize,
    #[clap(short, long, default_value = "Earth")]
    world: String,
    #[clap(short, long)]
    delete: bool,
}

impl Args {
    fn path(&self) -> Option<PathBuf> {
        let possible_paths = vec![
            PathBuf::from(format!("/pro/Minecraft/{}/region", self.world)),
            PathBuf::from(format!("/pro/Minecraft/{}/DIM-1/region", self.world)),
            PathBuf::from(format!("/pro/Minecraft/{}/DIM1/region", self.world)),
        ];
        for path in possible_paths {
            if path.is_dir() {
                return Some(path);
            }
        }
        None
    }
}

#[derive(Debug)]
struct Coord {
    path: PathBuf,
    x: isize,
    z: isize,
}

impl Coord {
    fn new(path: &Path) -> Self {
        let mut split = path.file_name().unwrap().to_str().unwrap().split('.');
        split.next().unwrap();
        let x = split.next().unwrap();
        let z = split.next().unwrap();

        Coord {
            path: path.to_path_buf(),
            x: x.parse().unwrap(),
            z: z.parse().unwrap(),
        }
    }
    fn to_delete(&self, args: &Args) -> bool {
        self.x.abs() as usize > (args.x / 512) || self.z.abs() as usize > (args.z / 512)
    }
}

impl fmt::Display for Coord {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "File: {}\nX:{}\nZ:{}\nx:{}\nz:{}\n",
            self.path.display(),
            self.x,
            self.z,
            self.x * 512,
            self.z * 512
        )
    }
}

fn delete_regions(args: &Args, coords: &[Coord]) -> Result<()> {
    if args.delete {
        for coord in coords {
            fs::remove_file(&coord.path)?;
        }
        println!("Deletion complete");
    }
    Ok(())
}

fn main() -> Result<()> {
    let args = Args::parse();
    let path = match args.path() {
        Some(x) => x,
        None => bail!("No path found."),
    };
    let iter = fs::read_dir(path)?;
    let files: Vec<PathBuf> = iter.map(|x| x.unwrap().path()).collect();
    let coords: Vec<Coord> = files
        .iter()
        .map(|f| Coord::new(f))
        .filter(|c| c.to_delete(&args))
        .collect();

    println!("Regions found: {}", coords.len());

    delete_regions(&args, &coords)?;
    Ok(())
}
